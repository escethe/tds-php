<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;
        ?>

        <p>
            <?php
                $utilisateur = [
                        'nom' => 'Shirou',
                        'prenom' => 'Fighken',
                        'login' => 'shirouf'
                ];
                //var_dump($utilisateur);
                if (count($utilisateur)) echo "Il n'y a aucun utilisateur";
                echo "ModeleUtilisateur $utilisateur[prenom] $utilisateur[nom] de login $utilisateur[login]";
            ?>
        </p>
    </body>
</html>