<?php
require_once "Trajet.php";

if (empty($_POST)){
    echo "Aucun résultat";
}
else {
    //var_dump($_GET);
    $tra = new Trajet(null, $_POST["depart"], $_POST["arrivee"], new DateTime($_POST["date"]), $_POST["prix"], 
        ModeleUtilisateur::recupererUtilisateurParLogin($_POST["conducteurLogin"]), $_POST["nonFumeur"]);
    $tra->ajouter();

    $trajets = Trajet::recupererTrajets();
    foreach ($trajets as $trajet) {
        if ($trajet->getConducteur()->getLogin() == $_POST["conducteurLogin"]) {
            echo $trajet;
        }
    }
}