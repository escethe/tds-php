<?php
$controleur = \App\Covoiturage\Lib\PreferenceControleur::lire();
$isUtilisateur = ($controleur == 'utilisateur');
$isTrajet = ($controleur == 'trajet');
?>

<form method="get" action="../web/ControleurFrontal.php">
    <fieldset>
        <legend>Mes préférences :</legend>
        <input type='hidden' name='action' value='enregistrerPreference'>

        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" value="trajet" <?php //if($isUtilisateur) echo 'checked'; ?>>
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php //if($isTrajet) echo 'checked'; ?>>
        <label for="trajetId">Trajet</label>
        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>