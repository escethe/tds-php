<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        if ($dureeExpiration == null) $dureeExpiration = 0;
        setcookie($cle, serialize($valeur), $dureeExpiration);
    }

    public static function lire(string $cle) : mixed {
        if (Cookie::contient($cle)) {
            return unserialize($_COOKIE[$cle]);
        }
        return "aucun cookie à cette clé";
    }

    public static function contient(string $cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer(string $cle) : void {
        if (Cookie::contient($cle)) {
            unset($_COOKIE["TestCookie"]);
        }
    }
}