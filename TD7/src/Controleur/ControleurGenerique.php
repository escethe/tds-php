<?php

namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);// Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    /*public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php',["titre"=>"Erreur","messageErreur" => $messageErreur,"cheminCorpsVue" => "utilisateur/erreur.php"]);
    }*/

    protected static function afficherFormulairePreference() : void {
        self::afficherVue("../vue/vueGenerale.php", [
            "titre" => "Formulaire des Preferences",
            "cheminCorpsVue" => "formulairePreference.php",

        ]);
    }
}