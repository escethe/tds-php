<?php
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\Modele\HTTP\Session;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

/******************************************************/
//Champ pour exécuter des commandes

/******************************************************/

$controleur = $_GET['controleur'] ?? PreferenceControleur::lire() ?? 'utilisateur';
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);

$action = $_GET['action'] ?? 'afficherListe';
// Appel de la méthode statique $action de ControleurUtilisateur
if (class_exists($nomDeClasseControleur)) {
    if(!in_array($action,get_class_methods($nomDeClasseControleur)))
        $nomDeClasseControleur::afficherErreur("La page n'existe pas");
    else
        $nomDeClasseControleur::$action();
} else
    ControleurUtilisateur::afficherErreur("Controleur " . $nomDeClasseControleur . " non trouve");