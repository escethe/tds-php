<?php
namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Trajet;

class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $mdpHache;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommeConducteur;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

// un constructeur
    public function __construct(
        string $login,
        string $mdpHache,
        string $nom,
        string $prenom
    )
    {
        $this->setLogin($login);
        $this->mdpHache  = $mdpHache;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
        $this->trajetsCommeConducteur = null;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

// un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }


    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function getTrajetsCommePassager(): array
    {
        if (is_null($this->trajetsCommePassager))
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);

        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public function getTrajetsCommeConducteur(): ?array
    {
        if (is_null($this->trajetsCommeConducteur))
            $this->trajetsCommeConducteur = $this->recupererTrajetsCommeConducteur();

        return $this->trajetsCommeConducteur;
    }

    public function setTrajetsCommeConducteur(?array $trajetsCommeConducteur): void
    {
        $this->trajetsCommeConducteur = $trajetsCommeConducteur;
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommeConducteur(): array
    {
        $sql = "SELECT * FROM trajet
WHERE conducteurLogin = '$this->login'";

        $trajets = [];
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        foreach ($pdoStatement as $trajet) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajet);
        }

        return $trajets;
    }

}

?>