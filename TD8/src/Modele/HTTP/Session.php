<?php
namespace App\Covoiturage\Modele\HTTP;

use App\Covoiturage\Configuration\ConfigurationSite;
use Exception;

class Session
{
    private static ?Session $instance = null;
    private static int $dureeExpiration = 0;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        if (session_start() === false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }
        $this->verifierDerniereActivite();
    }

    public static function getInstance(): Session
    {
        if (is_null(Session::$instance))
            Session::$instance = new Session();
        return Session::$instance;
    }

    private function verifierDerniereActivite(){
        if (isset($_SESSION['derniere_activite'])) {
            $inactivite = time() - $_SESSION['derniere_activite'];  // Temps d'inactivité
            if ($inactivite > ConfigurationSite::DUREE_EXPIRATION_SESSION) //Si la session a expiré
                session_unset();
        }
        //Remet le compteur à zero
        $_SESSION['derniere_activite'] = time();
    }

    public function contient($nom): bool
    {
        return array_key_exists($nom, $_SESSION);
    }

    public function enregistrer(string $nom, $valeur): void
    {
        $_SESSION[$nom] = $valeur;
    }

    public function lire(string $nom): ?string
    {
        if ($this->contient($nom))
            return $_SESSION[$nom];
        return null;
    }

    public function supprimer($nom): void
    {
        unset($_SESSION[$nom]);
    }

    public function detruire() : void
    {
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        Cookie::supprimer(session_name()); // deletes the session cookie
        // Il faudra reconstruire la session au prochain appel de getInstance()
        Session::$instance = null;
    }
}