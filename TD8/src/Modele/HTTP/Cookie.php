<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, $valeur, ?int $dureeExpiration = null): void
    {
        $valeur = json_encode($valeur);
        $expiration = $dureeExpiration !== null ? time() + $dureeExpiration : 0;
        setcookie($cle, $valeur, $expiration);
    }

    public static function lire(string $cle) : ?String
    {
        return isset($_COOKIE[$cle]) ? json_decode($_COOKIE[$cle]) : null;
    }

    public static function contient($cle) : bool {
        return array_key_exists($cle, $_COOKIE);
    }

    public static function supprimer(string $cle) : void
    {
        unset($_COOKIE[$cle]);
    }
}