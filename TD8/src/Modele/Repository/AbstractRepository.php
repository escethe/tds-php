<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

abstract class AbstractRepository
{

    function debugQuery($sql, $params) {
        echo "<p>$sql</p>\n";
        foreach ($params as $key => $value) {
            // Ajouter des guillemets pour les valeurs de type string
            $value = is_string($value) ? "'$value'" : $value;
            // Remplacer les tags par les valeurs
            $sql = str_replace(":$key", $value, $sql);
        }
        echo "<p>$sql</p>\n";
    }

    public function mettreAJour(AbstractDataObject $objet): void
    {
        $colonnesAvecTags = join(", ", array_map(
            fn($col) => "$col=:{$col}Tag",
            static::getNomsColonnes()
        ));
        $clePrimaire = static::getNomClePrimaire();

        $sql = "UPDATE ".static::getNomTable()." SET $colonnesAvecTags
                WHERE $clePrimaire=:".$clePrimaire."Tag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = static::formatTableauSQL($objet);

        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $colonnes = join(", ",static::getNomsColonnes());
        $placeholders = join(", ", array_map(fn($col) => ":{$col}Tag", static::getNomsColonnes()));

        $sql = "INSERT INTO ". static::getNomTable() ." ($colonnes) VALUES ($placeholders)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = static::formatTableauSQL($objet);

        try {
            $pdoStatement->execute($values);
        } catch (PDOException $e) {
            return false;
        }

        return true;
    }

    public function supprimerParClePrimaire(string $cle): void
    {
        $sql = "DELETE FROM ".static::getNomTable()." WHERE ".static::getNomClePrimaire()." = :cle";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array('cle' => $cle);
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $cle): ?AbstractDataObject
    {
        $sql = "SELECT * from ".static::getNomTable()." WHERE ".static::getNomClePrimaire()."=:cle";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(':cle' => $cle);
        $pdoStatement->execute($values);
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau)
            return null;
        else
            return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function recuperer(): array
    {
        $sql = "SELECT * FROM " . static::getNomTable();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        $tableau = [];
        foreach ($pdoStatement as $objetFormatTableau) {
            $tableau[] = $this->construireDepuisTableauSQL($objetFormatTableau);
        }

        return $tableau;
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

}