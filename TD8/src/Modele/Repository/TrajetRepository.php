<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class TrajetRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $trajetTableau): Trajet
    {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );
        $trajet->setPassagers(self::recupererPassagers($trajet));

        return $trajet;
    }

    /**
     * @return Utilisateur[]
     */
    public static function recupererPassagers(Trajet $trajet): array
    {
        $sql = "SELECT passagerLogin FROM trajet t
                JOIN passager p ON t.id = p.trajetId
                WHERE trajetId = :trajetId";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array("trajetId" => $trajet->getId());
        $pdoStatement->execute($values);

        $passagers = [];
        foreach ($pdoStatement as $ligne) {
            $passagers[] = (new UtilisateurRepository())->recupererParClePrimaire($ligne["passagerLogin"]);
        }

        return $passagers;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee","date","prix","conducteurLogin","nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur() !== null
        );
    }
}