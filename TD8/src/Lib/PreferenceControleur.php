<?php
namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Cookie;

class PreferenceControleur {
    private static string $clePreference = "preferenceControleur";

    public static function enregistrer(string $preference) : void
    {
        Cookie::enregistrer(self::$clePreference, $preference);
    }

    public static function lire() : ?string
    {
        $cle = self::$clePreference;
        return Cookie::lire($cle);

    }

    public static function existe() : bool
    {
        $cle = self::$clePreference;
        return Cookie::contient($cle);
    }

    public static function supprimer() : void
    {
        $cle = self::$clePreference;
        Cookie::supprimer($cle);
    }
}