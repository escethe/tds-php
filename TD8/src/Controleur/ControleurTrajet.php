<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", [
            "trajets" => $trajets,
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/liste.php",
        ]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue("vueGenerale.php", [
            "messageErreur" => $messageErreur,
            "titre" => "Erreur",
            "cheminCorpsVue" => "trajet/erreur.php"
        ]);
    }

    public static function afficherDetail(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if ($trajet == null)
            self::afficherVue("vueGenerale.php", [
                "messageErreur" => "Le trajet n'existe pas",
                "titre" => "Page d'erreur",
                "cheminCorpsVue" => "trajet/erreur.php",
            ]);
        else
            self::afficherVue("vueGenerale.php", [
                "trajet" => $trajet,
                "titre" => "Details trajet",
                "cheminCorpsVue" => "trajet/detail.php",
            ]);
    }

    public static function supprimer(): void
    {
        (new TrajetRepository())->supprimerParClePrimaire($_GET['id']);

        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", [
            "id" => $_GET['id'],
            "trajets" => $trajets,
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/trajetSupprime.php",
        ]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);

        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", [
            "trajets" => $trajets,
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/trajetCree.php",
        ]);
    }

    public static function mettreAJour(){
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);

        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue("vueGenerale.php", [
            "id" => $trajet->getId(),
            "trajets" => $trajets,
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/trajetMisAJour.php",
        ]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php", [
            "titre" => "Formulaire de creation",
            "cheminCorpsVue" => "trajet/formulaireCreation.php"
        ]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        self::afficherVue("vueGenerale.php", [
            "trajet" => $trajet,
            "titre" => "Formulaire de mise a jour",
            "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"
        ]);
    }

    public static function construireDepuisFormulaire(array $tableauDonnesFormulaire): Trajet
    {
        $conducteur = (new UtilisateurRepository)->recupererParClePrimaire($_GET["conducteurLogin"]);
        /** @var Utilisateur $conducteur */

        return new Trajet(
            $tableauDonnesFormulaire['id'] ?? null,
            $tableauDonnesFormulaire["depart"],
            $tableauDonnesFormulaire["arrivee"],
            new DateTime($tableauDonnesFormulaire["date"]),
            $tableauDonnesFormulaire["prix"],
            $conducteur,
            isset($tableauDonnesFormulaire["nonFumeur"])
        );
    }

}