<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository)->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
        ]);
    }

    public static function afficherDetail(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if ($utilisateur == null)
            ControleurUtilisateur::afficherVue("vueGenerale.php", [
                "messageErreur" => "L'utilisateur n'existe pas",
                "titre" => "Page d'erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
            ]);
        else
            ControleurUtilisateur::afficherVue("vueGenerale.php", [
                "utilisateur" => $utilisateur,
                "titre" => "Details utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php",
            ]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php", [
            "titre" => "Formulaire de creation",
            "cheminCorpsVue" => "utilisateur/formulaireCreation.php"
        ]);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue("vueGenerale.php", [
            "messageErreur" => $messageErreur,
            "titre" => "Erreur",
            "cheminCorpsVue" => "utilisateur/erreur.php"
        ]);
    }

    public static function supprimer(): void
    {
        (new UtilisateurRepository())->supprimerParClePrimaire($_GET['login']);

        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", [
            "login" => $_GET['login'],
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
        ]);
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        self::afficherVue("vueGenerale.php", [
            "utilisateur" => $utilisateur,
            "titre" => "Formulaire de mise a jour",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"
        ]);
    }

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVue("vueGenerale.php", [
            "titre" => "Authentification",
            "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"
        ]);
    }

    public static function creerDepuisFormulaire(): void
    {
        if($_GET["mdp"] != $_GET["mdp2"]){
            self::afficherErreur("Mots de passe distincts.");
            return;
        }
        $u = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($u);

        $utilisateurs = (new UtilisateurRepository)->recuperer();
        self::afficherVue("vueGenerale.php", [
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
        ]);
    }

    public static function mettreAJour()
    {
        /**
         * @var Utilisateur $u
         */
        $u = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if($_GET["ancien_mdp"] != $u->getMdpHache()){
            self::afficherErreur("L'ancien mot de passe n'est pas correct");
            return;
        }
        if($_GET["mdp"] != $_GET["mdp2"]){
            self::afficherErreur("Mots de passes distincts.");
            return;
        }

        $u = self::construireDepuisFormulaire($_GET);

        (new UtilisateurRepository)->mettreAJour($u);

        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", [
            "login" => $_GET['login'],
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
        ]);
    }

    public static function connecter(){
        if(!isset($_GET["login"]) || !isset($_GET["mdp"])){
            self::afficherErreur("Login et/ou mot de passe manquant.");
            return;
        }
        /**@var Utilisateur $utilisateur*/
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if(!isset($utilisateur)){
            self::afficherErreur("Login et/ou mot de passe incorrect.");
            return;
        }

        ConnexionUtilisateur::connecter($utilisateur->getLogin());

        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "utilisateur" => $utilisateur,
            "titre" => "Details utilisateur",
            "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php",
        ]);
    }

    public static function deconnecter(){
        ConnexionUtilisateur::deconnecter();

        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue("vueGenerale.php", [
            "utilisateurs" => $utilisateurs,
            "titre" => "Details utilisateur",
            "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php",
        ]);
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $t = $tableauDonneesFormulaire;
        return new Utilisateur($t['login'], $t['mdp'], $t['nom'], $t['prenom']);
    }

    public static function deposerCookie(string $nom, $valeur, int $dureeExpiration = null) : void
    {
        Cookie::enregistrer($nom,$valeur,$dureeExpiration);
    }

    public static function lireCookie(string $nom)
    {
        return Cookie::lire($nom);
    }

}
