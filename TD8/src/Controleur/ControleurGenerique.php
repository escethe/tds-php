<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference(){
        self::afficherVue("vueGenerale.php", [
            "titre" => "Formulaire de Preferences",
            "cheminCorpsVue" => "formulairePreference.php"
        ]);
    }

    public static function enregistrerPreference(){
        $controleurParDefaut = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($controleurParDefaut);
        self::afficherVue("vueGenerale.php", [
            "titre" => "Confirmation",
            "cheminCorpsVue" => "preferenceEnregistree.php"
        ]);
    }

}