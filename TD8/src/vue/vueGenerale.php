<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="../ressources/css/styles.css">
    <meta charset="UTF-8">
    <title><?php /** @var string $titre */

        use App\Covoiturage\Lib\ConnexionUtilisateur;

        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="ControleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="ControleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="ControleurFrontal.php?action=afficherFormulairePreference"><img alt="heart" src="../ressources/images/heart.png"></a>
            </li>
            <?php
            if(!ConnexionUtilisateur::estConnecte()) {
                echo "
            <li>
                <a href='ControleurFrontal.php?action=afficherFormulaireCreation'><img alt='Créer un compte' src='../ressources/images/add-user.png'></a>
            </li>
            <li>
                <a href='ControleurFrontal.php?action=afficherFormulaireConnexion'><img alt='Se connecter' src='../ressources/images/enter.png'></a>
            </li>\n";
            } else {
                $login = ConnexionUtilisateur::getLoginUtilisateurConnecte();
                echo "
            <li>
                <a href='ControleurFrontal.php?action=afficherDetail&login=$login'><img alt='Profil Utilisateur' src='../ressources/images/user.png'></a>
            </li>
            <li>
                <a href='ControleurFrontal.php?action=deconnecter'><img alt='Déconnexion' src='../ressources/images/logout.png'></a>
            </li>\n";
            }
            ?>
            
        </ul>
    </nav>
</header>
<main>
    <?php
    /** @var string $cheminCorpsVue */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>Site de covoiturage de Cococovoitures</p>
</footer>
</body>
</html>