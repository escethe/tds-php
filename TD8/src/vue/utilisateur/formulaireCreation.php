<form method="get" action="../web/ControleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='controleur' value='utilisateur'>
        <input type='hidden' name='action' value='creerDepuisFormulaire'>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom*</label>
            <input class="InputAddOn-field" type="text" placeholder="Leblanc" name="nom" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom*</label>
            <input class="InputAddOn-field" type="text" placeholder="Juste" name="prenom" id="prenom_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login*</label>
            <input class="InputAddOn-field" type="text" placeholder="leblancj" name="login" id="login_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe*</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe*</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>