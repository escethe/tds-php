<?php
/** @var Utilisateur $utilisateur */
use App\Covoiturage\Modele\DataObject\Utilisateur;
$prenom = htmlspecialchars($utilisateur->getPrenom());
$nom = htmlspecialchars($utilisateur->getNom());
$login = htmlspecialchars($utilisateur->getLogin());
echo "    <p>$prenom $nom avec le login $login</p>\n";