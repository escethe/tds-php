<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;

$prefix = "../web/ControleurFrontal.php?controleur=utilisateur&action=";

/** @var Utilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo "    <p>Utilisateur de login <a href='{$prefix}afficherDetail&login=$loginURL'>$loginHTML</a> : <a href='{$prefix}supprimer&login=$loginURL'>Supprimer</a> <a href='{$prefix}afficherFormulaireMiseAJour&login=$loginURL'>Modifier</a>.</p>\n";
    echo "<hr>\n";
}
echo "<a href='{$prefix}afficherFormulaireCreation'>Créer un utilisateur</a>\n";