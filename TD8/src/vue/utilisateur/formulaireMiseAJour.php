<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
?>

<form method="get" action="../web/ControleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="hidden" name='controleur' value="utilisateur">
        <input type='hidden' name='action' value='mettreAJour'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
            <input class="InputAddOn-field" type="text" value="<?=$loginHTML?>" name="login" id="login_id" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom*</label>
            <input class="InputAddOn-field" type="text" placeholder="Leblanc" value="<?=$nomHTML?>" name="nom" id="nom_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prénom*</label>
            <input class="InputAddOn-field" type="text" placeholder="Juste" value="<?=$prenomHTML?>" name="prenom" id="prenom_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="ancien_mdp_id">Ancien mot de passe*</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="ancien_mdp" id="ancien_mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau Mot de passe*</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau mot de passe*</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>