<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

$prefix = "../web/ControleurFrontal.php?controleur=trajet&action=";

foreach ($trajets as $t) {
    $id = $t->getId();
    $idHTML = htmlspecialchars($id);
    $idURL = rawurlencode($id);
    echo "    <p>Trajet d'id $idHTML : <a href='{$prefix}afficherDetail&id=$idURL'>Details</a> <a href='{$prefix}supprimer&id=$idURL'>Supprimer</a> <a href='{$prefix}afficherFormulaireMiseAJour&id=$idURL'>Modifier</a>.</p>\n";
    echo "<hr>\n";
}
echo "<a href='{$prefix}afficherFormulaireCreation'>Créer un trajet</a>\n";