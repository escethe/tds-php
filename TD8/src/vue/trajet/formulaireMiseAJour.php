<?php
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var Trajet $trajet */
$departHTML = htmlspecialchars($trajet->getDepart());
$arriveeHTML = htmlspecialchars($trajet->getArrivee());
$dateHTML = htmlspecialchars($trajet->getDate()->format("Y-m-d"));
$prixHTML = htmlspecialchars($trajet->getPrix());
$conducteurLoginHTML = htmlspecialchars($trajet->getConducteur()->getLogin());
$nonFumeur = $trajet->isNonFumeur() ? "checked" : "";
?>

<form method="get" action="../web/ControleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="hidden" name="action" value="mettreAJour">
        <input type="hidden" name="controleur" value="trajet">
        <input type="hidden" name="id" value="<?=$trajet->getId()?>">
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart</label>
            <input class="InputAddOn-field" type="text" value="<?=$departHTML?>" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée</label>
            <input class="InputAddOn-field" type="text" value="<?=$arriveeHTML?>" placeholder="Sète" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date</label>
            <input class="InputAddOn-field" type="date" value="<?=$dateHTML?>" placeholder="JJ/MM/AAAA" name="date" id="date_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix</label>
            <input class="InputAddOn-field" type="number" value="<?=$prixHTML?>" placeholder="20" name="prix" id="prix_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur</label>
            <input class="InputAddOn-field" type="text" value="<?=$conducteurLoginHTML?>" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?</label>
            <input class="InputAddOn-field" type="checkbox" <?=$nonFumeur?> name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>