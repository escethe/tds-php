<?php
/** @var Trajet $trajet */

use App\Covoiturage\Modele\DataObject\Trajet;

$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : " ";
$date = htmlspecialchars($trajet->getDate()->format('d/m/Y'));
$depart = htmlspecialchars($trajet->getDepart());
$arrivee = htmlspecialchars($trajet->getArrivee());
$prenom = htmlspecialchars($trajet->getConducteur()->getPrenom());
$nom = htmlspecialchars($trajet->getConducteur()->getNom());

echo "Le trajet$nonFumeur du $date partira de $depart pour aller à $arrivee (conducteur: $prenom $nom).";