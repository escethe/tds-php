<?php
require_once __DIR__ . '/../Modele/ModeleUtilisateur.php'; // chargement du modèle
class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Charge la vue
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('utilisateur/liste.php', ['utilisateurs'=>$utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if (!isset($_GET['login'])) {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
        }
        else {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == null) ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
            else ControleurUtilisateur::afficherVue('utilisateur/detail.php', ['utilisateur'=>$utilisateur]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        if (isset($_GET['login'])) {
            $utilisateur  = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
            $utilisateur->ajouter();
            ControleurUtilisateur::afficherListe();
        }
    }
}
?>