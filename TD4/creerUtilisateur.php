<?php
require_once "Modele/ModeleUtilisateur.php";

if (empty($_POST)){
    echo "Aucun résultat";
}
else {
    //var_dump($_GET);
    $uti = new ModeleUtilisateur($_POST["login"], $_POST["nom"], $_POST["prenom"]);
    $uti->ajouter();
    $u = ModeleUtilisateur::recupererUtilisateurParLogin($uti->getLogin());
    echo $u;
}