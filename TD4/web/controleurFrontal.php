<?php
require_once __DIR__ . '/../src/Controleur/ControleurUtilisateur.php';
// On récupère l'action passée dans l'URL
$action = null;
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    // Appel de la méthode statique $action de ControleurUtilisateur
    ControleurUtilisateur::$action();
}
?>