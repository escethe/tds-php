<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détail de l'utilisateur</title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $utilisateur */

use App\Covoiturage\Modele\ModeleUtilisateur;

$detailUtilisateur = (string) $utilisateur;
echo htmlspecialchars($detailUtilisateur);
?>
</body>
</html>

