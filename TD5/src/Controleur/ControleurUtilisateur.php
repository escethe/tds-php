<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;

class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Charge la vue
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,
            "titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if (!isset($_GET['login'])) {
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre"=>"Erreur", "cheminCorpsVue"=>"utilisateur/erreur.php"]);
        }
        else {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == null) ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre"=>"Erreur", "cheminCorpsVue"=>"utilisateur/erreur.php"]);
            else ControleurUtilisateur::afficherVue("vueGenerale.php", ["utilisateur"=>$utilisateur,
                "titre"=>"Détail de l'utilisateur", "cheminCorpsVue"=>"utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre"=>"Formulaire",
            "cheminCorpsVue"=>"utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void {
        if (isset($_GET['login'])) {
            $utilisateur  = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
            $utilisateur->ajouter();

            ControleurUtilisateur::afficherListe();
        }
    }
}
?>