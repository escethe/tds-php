<?php

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');


use App\Covoiturage\Controleur;


// On récupère l'action passée dans l'URL
$appelControleur = "App\\Covoiturage\\Controleur\\ControleurUtilisateur";
$action = null;
if (isset($_GET['controleur']) && class_exists("App\\Covoiturage\\Controleur\\Controleur" . ucfirst($_GET['controleur']))) {
    $appelControleur = "App\\Covoiturage\\Controleur\\Controleur" . ucfirst($_GET['controleur']);
}
if (isset($_GET['action']) && in_array($_GET['action'], get_class_methods($appelControleur), false)) {
    $action = $_GET['action'];
}
else {
    $action = "afficherListe";
}
// Appel de la méthode statique $action
$appelControleur::$action();

?>