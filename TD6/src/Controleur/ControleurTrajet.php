<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;

class ControleurTrajet {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Charge la vue
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurTrajet::afficherVue('vueGenerale.php', ["trajets"=>$trajets,
            "titre"=>"Liste des trajets", "cheminCorpsVue"=>"trajet/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        ControleurTrajet::afficherVue("vueGenerale.php", ["messageErreur"=>$messageErreur,
            "titre"=>"Erreur", "cheminCorpsVue"=>"trajet/erreur.php"]);
    }
}