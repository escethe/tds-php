<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Charge la vue
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["utilisateurs"=>$utilisateurs,
            "titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if (!isset($_GET['login'])) {
            ControleurUtilisateur::afficherErreur();
        }
        else {
            $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == null) ControleurUtilisateur::afficherErreur();
            else ControleurUtilisateur::afficherVue("vueGenerale.php", ["utilisateur"=>$utilisateur,
                "titre"=>"Détail de l'utilisateur", "cheminCorpsVue"=>"utilisateur/detail.php"]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre"=>"Formulaire",
            "cheminCorpsVue"=>"utilisateur/formulaireCreation.php"]);
    }

    public static function afficherFormulaireMiseAJour(){
        if (isset($_GET['login'])) {
            $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == null) ControleurUtilisateur::afficherErreur("Utilisateur introuvable, 
                impossible de supprimer un utilisateur");
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["titre" => "Formulaire Mise A jour",
                "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
                "utilisateur" => $utilisateur]);
        }
        else {
            ControleurUtilisateur::afficherErreur();
        }
    }

    public static function creerDepuisFormulaire() : void {
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $utilisateur  = new Utilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
            UtilisateurRepository::ajouter($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["cheminVueApresCree"=>"liste.php",
                "login"=>$utilisateur->getLogin(), "utilisateurs"=>$utilisateurs,
                "cheminCorpsVue"=>"utilisateur/utilisateurCree.php", "titre"=>"Utilisateur ajout"]);
        }
        else {
            ControleurUtilisateur::afficherErreur();
        }
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        ControleurUtilisateur::afficherVue("vueGenerale.php", ["messageErreur"=>$messageErreur,
            "titre"=>"Erreur", "cheminCorpsVue"=>"utilisateur/erreur.php"]);
    }

    public static function supprimerUtilisateur() : void {
        if (isset($_GET['login'])) {
            $utilisateur = UtilisateurRepository::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == null) ControleurUtilisateur::afficherErreur("Utilisateur introuvable, 
                impossible de supprimer un utilisateur");
            UtilisateurRepository::supprimerParLogin($utilisateur->getLogin());
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["cheminVueApresSupprime"=>"liste.php",
                "login"=>$utilisateur->getLogin(), "utilisateurs"=>$utilisateurs,
                "cheminCorpsVue"=>"utilisateur/utilisateurSupprime.php", "titre"=>"Utilisateur supprimé"]);
        }
    }

    public static function mettreAJourDepuisFormulaire() : void {
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $utilisateur  = UtilisateurRepository::recupererUtilisateurParLogin($_GET['login']);
            if ($utilisateur == null) ControleurUtilisateur::afficherErreur("Utilisateur introuvable, 
                impossible de le mettre à jour");
            $nvNomUtilisateur = $_GET['nom'];
            $nvPrenomUtilisateur = $_GET['prenom'];
            UtilisateurRepository::mettreAJour($utilisateur, $nvNomUtilisateur, $nvPrenomUtilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            ControleurUtilisateur::afficherVue("vueGenerale.php", ["cheminVueApresCree"=>"liste.php",
                "login"=>$utilisateur->getLogin(), "utilisateurs"=>$utilisateurs,
                "cheminCorpsVue"=>"utilisateur/utilisateurMisAJour.php", "titre"=>"Utilisateur à jour"]);
        }
        else {
            ControleurUtilisateur::afficherErreur();
        }
    }
}
?>