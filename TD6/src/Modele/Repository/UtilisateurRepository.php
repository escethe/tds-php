<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;


class UtilisateurRepository extends AbstractRepository
{
    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    public static function recupererUtilisateurs() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $tabUtilisateurs = [];
        foreach ($pdoStatement as $utilisateursql) {
            $utilisateur = new Utilisateur($utilisateursql["loginBaseDeDonnees"], $utilisateursql["nomBaseDeDonnees"], $utilisateursql["prenomBaseDeDonnees"]);
            $tabUtilisateurs[] = $utilisateur;
        }
        return $tabUtilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE loginBaseDeDonnees = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau) return null;
        return (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public static function ajouter(Utilisateur $utilisateur) : void {
        $sql = "INSERT INTO utilisateur (loginBaseDeDonnees, nomBaseDeDonnees, prenomBaseDeDonnees) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
        $pdoStatement->execute($values);
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau) : Utilisateur
    {
        return new Utilisateur($objetFormatTableau["loginBaseDeDonnees"], $objetFormatTableau["nomBaseDeDonnees"], $objetFormatTableau["prenomBaseDeDonnees"]);
    }

    public static function supprimerParLogin(string $login) : void {
        $sql = "DELETE FROM utilisateur WHERE loginBaseDeDonnees = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $login
        );
        $pdoStatement->execute($values);
    }

    public static function mettreAJour(Utilisateur $utilisateur, string $nvNom, string $nvPrenom)  : void {
        $sql = "UPDATE utilisateur 
                SET nomBaseDeDonnees = :nomTag, prenomBaseDeDonnees = :prenomTag
                WHERE loginBaseDeDonnees = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $nvNom,
            "prenomTag" => $nvPrenom,
        );
        $pdoStatement->execute($values);
    }
}