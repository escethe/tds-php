<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;
abstract class AbstractRepository
{
    protected abstract function getNomTable(): string;

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $tableau = [];
        foreach ($pdoStatement as $lignesql) {
            $ligne = $this->construireDepuisTableauSQL($lignesql);
            $tableau[] = $ligne;
        }
        return $tableau;
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;


}