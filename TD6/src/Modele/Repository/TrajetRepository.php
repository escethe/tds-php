<?php


namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository{

    protected function getNomTable(): string
    {
        return "trajets";
    }


    public function ajouter(Trajet $trajet) : void {
        $sql = "INSERT INTO trajets (depart, arrivee, la_date, prix, conducteurLogin, nonFumeur) VALUES (:departTag, :arriveeTag, :la_dateTag, :prixTag, :conducteurLoginTag, :nonFumeurTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $nonFumeur = 1;
        if (!$this->nonFumeur) $nonFumeur = 0;
        $values = array(
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "la_dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $nonFumeur
        );
        $pdoStatement->execute($values);
    }

    /**
     * @return Utilisateur[]
     */
    public static function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT passagerId FROM passager WHERE trajetId = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "trajetId" => $trajet->getId()
        );
        //$utilisateur  = $pdoStatement->fetch();
        $pdoStatement->execute($values);

        $passagers = [];

        foreach($pdoStatement as $passagerFormatTableau) {
            $passagers = UtilisateurRepository::recupererUtilisateurParLogin($passagerFormatTableau["passagerId"]);
        }

        return $passagers;
    }

    /**
     * utilise la méthode de la classe abstraite, à partir d'une nouvelle instance anonyme
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        return (new TrajetRepository())->recuperer();
    }

    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["la_date"]),
            $trajetTableau["prix"],
            UtilisateurRepository::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );
        $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
        return $trajet;
    }
}