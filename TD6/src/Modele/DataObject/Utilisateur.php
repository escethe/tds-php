<?php
namespace App\Covoiturage\Modele\DataObject;
class Utilisateur extends AbstractDataObject {

    private string $login;
    private string $nom;
    private string $prenom;

    public function getNom(){
        return $this->nom;
    }

    public function setNom(string $nom){
        $this->nom = $nom;
    }

    public function getPrenom() : string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom){
        $this->prenom = $prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    public function setLogin(string $login){
        $this->login = substr($login, 0, 64);
    }

    public function __construct(string $login, string $nom, string $prenom){
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function __toString() : string {
        return "Utilisateur $this->prenom $this->nom de login $this->login";
    }
/*
    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau["loginBaseDeDonnees"], $utilisateurFormatTableau["nomBaseDeDonnees"], $utilisateurFormatTableau["prenomBaseDeDonnees"]);
    }

    public static function recupererUtilisateurs() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $tabUtilisateurs = [];
        foreach ($pdoStatement as $utilisateursql) {
            $utilisateur = new Utilisateur($utilisateursql["loginBaseDeDonnees"], $utilisateursql["nomBaseDeDonnees"], $utilisateursql["prenomBaseDeDonnees"]);
            $tabUtilisateurs[] = $utilisateur;
        }
        return $tabUtilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE loginBaseDeDonnees = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if (!$utilisateurFormatTableau) return null;
        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur (loginBaseDeDonnees, nomBaseDeDonnees, prenomBaseDeDonnees) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
        );
        $pdoStatement->execute($values);
    }*/

}
