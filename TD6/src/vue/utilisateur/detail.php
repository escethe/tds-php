<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Modele\DataObject\Utilisateur;

$detailUtilisateur = (string) $utilisateur;
echo htmlspecialchars($detailUtilisateur);
?>
