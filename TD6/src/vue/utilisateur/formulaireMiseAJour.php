<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/**
 * @var $utilisateur
 */
?>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJourDepuisFormulaire">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input class="InputAddOn-field" type="text" name="login" id="login_id"
            value="<?=htmlspecialchars($utilisateur->getLogin())?>" readonly/>
        </p>
        <p>
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?=htmlspecialchars($utilisateur->getPrenom())?>" name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <input class="InputAddOn-field" type="text" placeholder="<?=htmlspecialchars($utilisateur->getNom())?>" name="nom" id="nom_id" required/>
        </p>
        <p>
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>;
