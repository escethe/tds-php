<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <input class="InputAddOn-field" type="text" placeholder="sheazera" name="login" id="login_id" required/>
        </p>
        <p>
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <input class="InputAddOn-field" type="text" placeholder="Aymrett" name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <input class="InputAddOn-field" type="text" placeholder="Sheazer" name="nom" id="nom_id" required/>
        </p>
        <p>
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>