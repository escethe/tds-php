<?php
/**
 * @var string $cheminVueApresSupprime
 * @var string $login
 */
echo "L'utilisateur de login " . $login . " a bien été supprimé.";
require __DIR__ . "/{$cheminVueApresSupprime}";
?>
